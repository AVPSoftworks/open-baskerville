# Open Baskerville

## Authors and acknowledgment
Under that font worked previous maintainer https://github.com/klepas/open-baskerville and me new Volodymyr Artemenko.

## License
Open Baskerville Latin & Cyrillic font files are licensed under the GNU GPL version 3 (GNU General Public License) See 'COPYING-GPLv3.txt

## Thanks
